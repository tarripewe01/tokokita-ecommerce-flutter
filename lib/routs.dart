
import 'package:flutter/widgets.dart';
import 'package:toko_kita/screens/splash/splash_screen.dart';

final Map<String, WidgetBuilder> routes = {
  SplashScreen.routeName: (context) => SplashScreen(),
};
