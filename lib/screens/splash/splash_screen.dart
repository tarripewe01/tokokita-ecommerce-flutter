import 'package:flutter/material.dart';
import 'package:toko_kita/screens/splash/components/body.dart';
import 'package:toko_kita/size_config.dart';

class SplashScreen extends StatelessWidget {
  static String routeName = "/splash";
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: Body(),
    );
  }
}
